/**
 *
 */
package org.springframework.samples.petclinic.model;

import org.junit.Test;
import org.springframework.samples.petclinic.SampleClass;

/**
 * @author vibhor.r
 *
 *         Dec 16, 2016 : 1:04:51 PM
 *
 */
public class SampleClassTest {


    @Test
    public void doSomethingTest() {

        new SampleClass().doSomething();
    }
}
